from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello World!'


@app.errorhandler(404)
def not_found(e):
    return jsonify({'status':404, 'message':'Route Not Found'})


@app.route('/v1/guess')
def parameters():
    name = request.args.get('name')
    if name:
       return jsonify(message="Welcome, " + name + "!")
    else:
       return jsonify(message="Welcome, No Name!" )



@app.route('/v1/guess/<string:name>/<int:age>')
def url_variables(name: str, age: int):
    if age < 49:
        return jsonify(message="Sorry " + name + ", you are so young!")
    else:
        return jsonify(message="Welcome " + name + ", you are down hill!")

#   default app.run() listens on port 5000
if __name__ == '__main__':
    app.run(debug=True,host="0.0.0",port=8080)


