FROM python:3.11-slim-bullseye

RUN apt-get update && \
    apt-get install -y  apache2 libapache2-mod-wsgi-py3 && \
    a2enmod ssl && \
    useradd -m -d /home/appadmin -s /bin/bash appadmin && \
    mkdir /var/www/flaskapp /etc/apache2/conf /opt/WDIR && \
    cd /var/www/flaskapp; python -m venv venv && \
    source venv/bin/activate; pip install flask
COPY WDIR/* /var/www/flaskapp
COPY flaskapp.conf /etc/apache2/sites-enabled
COPY ssl.conf      /etc/apache2/mods-available
COPY flaskapp.crt flaskapp.key flaskapp-ca.crt  /etc/apache2/conf 

WORKDIR /opt/WDIR
EXPOSE 80 443

ENTRYPOINT ["/var/www/flaskapp/run.app"]
